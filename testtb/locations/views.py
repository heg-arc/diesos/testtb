from django.views.generic import CreateView, DetailView, ListView, UpdateView
from treebeard.forms import movenodeform_factory

from .models import Location


class LocationListView(ListView):
    model = Location
    context_object_name = "locations"


class LocationDetailView(DetailView):
    model = Location
    context_object_name = "location"


class LocationCreateView(CreateView):
    model = Location
    form_class = movenodeform_factory(Location)


class LocationUpdateView(UpdateView):
    model = Location
    form_class = movenodeform_factory(Location)
