from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from treebeard.mp_tree import MP_Node


class Location(MP_Node):
    name = models.CharField(verbose_name=_("location name"), max_length=225, unique=True)

    description = models.TextField(verbose_name=_("description"), null=True, blank=True)

    color = models.CharField(max_length=7, null=True, blank=True)  # Code couleur

    elevation = models.IntegerField(
        verbose_name=_("elevation"),
        null=True,
        blank=True,
    )

    # xxx.xxxxxx
    latitude = models.DecimalField(
        verbose_name=_("latitude"),
        max_digits=9,
        decimal_places=6,
        null=True,
        blank=True,
    )

    longitude = models.DecimalField(
        verbose_name=_("longitude"),
        max_digits=9,
        decimal_places=6,
        null=True,
        blank=True,
    )

    node_order_by = ["name"]  # Tri de noeuds

    def __str__(self):
        return f"{self.name}"

    class Meta:
        verbose_name = _("location")
        verbose_name_plural = _("locations")
        ordering = ["name"]

    def get_absolute_url(self):
        return reverse("location-detail", kwargs={"pk": self.pk})
